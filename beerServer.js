var fs = require("fs");
var express = require("express");
var mongoose = require("mongoose");
var app = express();
var router = express.Router();
var Beer = require('./models/beer');
var bodyParser = require("body-parser");   //In order to accept data via POST or PUT, we need to add another package called body-parser

mongoose.connect('mongodb://localhost:27017/beerlocker');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("Connected to mongodb");
});

app.use('/api', router);
app.use(bodyParser.urlencoded({
  extended: true
}));

router.get('/', function(req, res) {
    res.json({ message: 'You are running the app' });
});



app.listen("3000");


/******************************** Code to add beers ****************************** */

//There's some problem with this code
// var beersRoute = router.route('/beers');
// beersRoute.post(function(req, res) {
//   var beer = new Beer();
//   var headers = req.headers;
//   var userAgent = headers['user-agent'];
//   var url = req.url;
//   console.log("userAgent: "+userAgent);
//   console.log("Url: "+url);
//   console.log("The value in requestbody: "+req.body.name);
//   res.end("Hello");
// });

//To Add a beer object
app.post('/api/beers', function (req, res) {
  var beer = new Beer();

  // Set the beer properties that came from the POST data
  beer.name = req.body.name;
  beer.type = req.body.type;
  beer.quantity = req.body.quantity;

  // Save the beer and check for errors
  beer.save(function(err) {
    if (err)
      res.send(err);

    res.json({ message: 'Beer added to the locker!', data: beer });
  });
});

//To Fetch all beers
app.get("/api/beers", function(req, res) {
  // Use the Beer model to find all beer
  Beer.find(function(err, beers) {
    if (err)
      res.send(err);

    res.json(beers);
  });
});

//To Fetch a beer object by Id
app.get("/api/beers/:beer_id",function(req, res) {
  // Use the Beer model to find a specific beer
  Beer.findById(req.params.beer_id, function(err, beer) {
    if (err) {
        res.send(err);
    }
    res.json(beer);
  });
});

//To Update or remove we use PUT
app.put("/api/beers/:beer_id",function(req, res) {
  Beer.findById(req.params.beer_id, function(err, beer) {
    if (err) {
        res.send(err);
    }
    // Update the existing beer quantity
    beer.quantity = req.body.quantity;
    beer.save(function(err) {
      if (err) {
        res.send(err);
      }
      res.json(beer);
    });
  });
});

// EndPoint to delete
app.delete("/api/beers/:beer_id",function(req, res) {
  Beer.findByIdAndRemove(req.params.beer_id, function(err) {
    if (err)
      res.send(err);

    res.json({ message: 'Beer removed from the locker!' });
  });
});
