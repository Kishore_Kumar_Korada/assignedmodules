var express = require("express");
var fs = require("fs");
var http = require("http");
var path = require("path");
var mime = require('mime');

var app = express();

/* sending the data response from server the request */
app.get('/person', function (req, res, next) {
    //This below chunk is to send a json
    /*
        console.log("Inside /persion");
        res.json(getStrinfiedJsonValue());
    */

    //This below chunk is to download a file
    /*
        var file = __dirname + '/record.json';
        res.download(file); // Set disposition and send it.
    */
    res.sendFile('./record.json' , { root : __dirname});
});

app.get('/file', function (req, res, next) {
    var filePath = path.join(__dirname, 'record.json1');
    console.log("FilePath is: "+filePath);
    var fileName = path.basename(filePath);
    var mimeType = mime.lookup(filePath);
    var stat = fs.statSync(filePath);

    res.writeHead(200, {
          "Content-Type": mimeType,
          "Content-Disposition" : "attachment; filename=" + fileName,
          'connection': 'keep-alive',
          "Content-Length": stat.size
    });
    var readStream = fs.createReadStream(filePath);
    readStream.on('data', function(data) {
        res.write(data);
    });
    
    readStream.on('end', function() {
        res.end();        
    });
});

app.get('/movie', function (req, res, next) {
    var filePath = path.join(__dirname, 'record.json1');
    console.log("FilePath is: "+filePath);
    var fileName = path.basename(filePath);
    var mimeType = mime.lookup(filePath);
    var stat = fs.statSync(filePath);

    console.log("fileName: "+fileName+" mimeType: "+mimeType);
    res.writeHead(200, {
          "Content-Type": mimeType,
          "Content-Disposition" : "attachment; filename=" + fileName,
          'connection': 'keep-alive',
          "Content-Length": stat.size
    });
    var readStream = fs.createReadStream(filePath);
    // We replaced all the event handlers with a simple call to readStream.pipe()
    readStream.pipe(res);
});

// fs.open('Koala.jpg', 'r', function(err, fd) {
//     console.log("Insie open of fs.open");
//     fs.fstat(fd, function(err, stats) {

//         var bufferSize=stats.size  ,
//             chunkSize=512,
//             buffer=new Buffer(bufferSize),
//             bytesRead = 0;

//         while (bytesRead < bufferSize) {
//             if ((bytesRead + chunkSize) > bufferSize) {
//                 chunkSize = (bufferSize - bytesRead);
//             }

//             fs.read(fd, buffer, bytesRead, chunkSize, bytesRead, testCallback);
//             bytesRead += chunkSize;
//         }
//         console.log(buffer.toString('utf8'));
//         fs.close(fd);
//     });
// });

// var testCallback = function(err, bytesRead, buffer){
//     console.log('err : ' +  err);
// };

fs.open('Koala.jpg', 'r', function(status, fd) {
    if (status) {
        console.log("Status message: "+status.message);
        return;
    }
    var buffer = new Buffer(100);
    fs.read(fd, buffer, 0, 100, 0, function(err, num) {
        console.log("BuffSize: "+buffer.size);
        console.log("Buff: "+buffer.toString('utf8', 0, num));
    });
});

app.get('/manualBuffer', function (req, res, next) {
    var filePath = path.join(__dirname, 'Koala.jpg');
    console.log("FilePath is: "+filePath);
    var fileName = path.basename(filePath);
    var mimeType = mime.lookup(filePath);
    var stat = fs.statSync(filePath);

    var manualBufferSize = 512; //64byte
    var buffer = new Buffer(manualBufferSize);   //Reading files in 64byte sequence

    console.log("fileName: "+fileName+" mimeType: "+mimeType);
    console.log("Before Memory Usage: "+ process.memoryUsage());

    res.writeHead(200, {
          "Content-Type": mimeType,
          "Content-Disposition" : "attachment; filename=" + fileName,
          'connection': 'keep-alive',
          "Content-Length": stat.size,
          "Transfer-Encoding": "chunked"
    });

    fs.open(filePath, 'r', function(err, fd) {
        // fs.read(fd, buffer, 0, manualBufferSize, 0, function (err, bytesRead, buffer) {
        //     console.log("Error: "+err);
        //     console.log("Bytes Read: "+bytesRead);
        //     console.log("Buffer: "+buffer);
        //     console.log(buffer.toString('utf8', 0, bytesRead));
        //     res.write(buffer);
        //     res.end();
        // });

        var completeBufferSize = stat.size;
        var offset = 0;  //is the offset in the buffer to start writing at
        var length = 512; //is an integer specifying the number of bytes to read
        var position = 0;  //is an integer specifying where to begin reading from in the file. If position is null, data will be read from the current file position
        var buffer = new Buffer(completeBufferSize);

        console.log("Before while");
        while(position < completeBufferSize) {
            console.log("Inside while");
            console.log("Before read The Total buffer size: "+completeBufferSize);
            console.log("Before read The Total length: "+length);
            console.log("Before position is: "+position);

            fs.read(fd,buffer,offset,length,position,function(err,lengthh,bufferr){
                console.log("Inside fread");
            });         
        }

        // var bufferSize=stat.size;
        // var chunkSize=512;
        // var buffer=new Buffer(bufferSize);
        // var bytesRead = 0;

        // while (bytesRead < bufferSize) {
        //     if ((bytesRead + chunkSize) > bufferSize) {
        //         chunkSize = (bufferSize - bytesRead);
        //     }

        //     fs.read(fd, buffer, bytesRead, chunkSize, bytesRead, function (err, bytesRead, buffer){
        //         bytesRead += chunkSize;
        //         console.log(bytesRead);
        //     });
        //     res.write(buffer, 'UTF-8');
        //     res.end();
        // }        
    });
});

app.get('/manualBufferAnother', function (req, res, next) {
    var filePath = path.join(__dirname, 'Koala.jpg');
    console.log("FilePath is: "+filePath);
    var fileName = path.basename(filePath);
    var mimeType = mime.lookup(filePath);
    var stat = fs.statSync(filePath);

    console.log("fileName: "+fileName+" mimeType: "+mimeType);
    console.log("Before Memory Usage: "+ process.memoryUsage());

    res.writeHead(200, {
          "Content-Type": mimeType,
          "Content-Disposition" : "attachment; filename=" + fileName,
          'connection': 'keep-alive',
          "Content-Length": stat.size,
          "Transfer-Encoding": "chunked"
    });

    fs.open(filePath, 'r', function(err, fd) {
        var completeBufferSize = stat.size;
        var offset = 0;  //is the offset in the buffer to start writing at
        var length = 511; //is an integer specifying the number of bytes to read
        var position = 0;  //is an integer specifying where to begin reading from in the file. If position is null, data will be read from the current file position
        var buffer = new Buffer(completeBufferSize);

        console.log("Before while");
        buf(res,fd,offset,position,length,buffer,stat);        
    });
});

var buf = function(res,fd,offset,position,length,buffer,stat) {
    console.log("Inside buf");
    console.log("The Value of position: "+position);
    console.log("The value of buffer.length: "+buffer.length);
    console.log("The value of length: "+length);
    console.log("The value of offset: "+offset);    
    console.log("Actual File Size: "+stat.size);

    if(position+buffer.length < length) {
        console.log("inside if and position+buffer.length: "+(position+buffer.length)+" < length: "+length+" :: "+(position+buffer.length<length));
        fs.read(fd,buffer,offset,length,position,function(error,bytesRead,bufferr) {
            res.write(bufferr.slice(0,bytesRead));
            console.log("Bytes Read: "+bytesRead);
            position=position+bufferr.length;
            buf(res,fd,offset,position,length,bufferr,stat);
        })
    } else {
        console.log("inside else and position+buffer.length: "+(position+buffer.length)+" < length: "+length+" :: "+(position+buffer.length<length));
        fs.read(fd,buffer,offset,length,position,function(error,bytesRead,bufferr) {
            console.log("Bytes Read in else: "+bytesRead);
            res.end(bufferr.slice(0,bytesRead));
            fs.close(fd)
            // res.write(bufferr.slice(0,bytesRead));
            // position=position+bufferr.length;
            // buf(res,fd,offset,position,length,bufferr,stat);
        })
    }
}



var jsonFile = 'record.json';

function getStrinfiedJsonValue() {
    var data = [];
    for (i=0; i <10; i++){
    var obj =  { name: getRandomName(), date :  getRandomDate() };
        data.push(obj);
    }
    return data;
}


fs.readFile(jsonFile, function (err, data) {
    console.log("binaryData: "+data);
});

/* getting randorm name */
function getRandomName() {
    var name = "";
    var possible = "abcdefghijklmnopqrstuvwxyz";

    for( var i=0; i < 15; i++ ) {
        name += possible.charAt(Math.floor(Math.random() * possible.length));
    }   

    return name;
}

/* getting random date */
function getRandomDate() {
    var from = new Date(2017, 2, 31);
    var till = new Date(2017,3,9);

    var date =  new Date(from.getTime() + Math.random() * (till.getTime() - from.getTime()));
    return date.getFullYear()+"-"+date.getMonth()+"-"+date.getDate();
}

app.listen(3000);