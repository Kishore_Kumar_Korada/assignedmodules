var mongoose = require('mongoose');
// Define our user schema
var UserSchema = new mongoose.Schema({
  username: String,
  password: String
});


// Export the Mongoose model
module.exports = mongoose.model('User', UserSchema);