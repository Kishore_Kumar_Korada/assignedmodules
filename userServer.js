var fs = require("fs");
var express = require("express");
var mongoose = require("mongoose");
var app = express();
var router = express.Router();
var User = require('./models/user');
var bodyParser = require("body-parser");   //In order to accept data via POST or PUT, we need to add another package called body-parser

mongoose.connect('mongodb://localhost:27017/beerlocker');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("Connected to mongodb");
});

app.use('/api', router);
app.use(bodyParser.urlencoded({
  extended: true
}));

router.get('/', function(req, res) {
    res.json({ message: 'You are running the app' });
});
app.listen("3000");


/******************************** Code to play around with users ****************************** */
//To Add a User
app.post("/api/users",function(req, res) {
    console.log("userName: "+req.body.username);
    console.log("passowrd: "+req.body.password)
  var user = new User({
    username: req.body.username,
    password: req.body.password
  });

  user.save(function(err) {
    if (err) {
        console.log("Inside error");
        res.send(err);
    }
    res.json({ message: 'New User added' });
  });
});